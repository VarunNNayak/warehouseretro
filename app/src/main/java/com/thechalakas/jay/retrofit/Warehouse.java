package com.thechalakas.jay.retrofit;
/*
 * Created by jay on 23/09/17. 12:06 AM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Warehouse {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Warehouse_Name")
    @Expose
    private String warehouseName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

}