package com.thechalakas.jay.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WarehouseSend {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Warehouse_Name")
    @Expose
    private String warehouse_name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWarehouse_Name() {
        return warehouse_name;
    }

    public void setWarehouse_Name(String warehouse_name) {
        this.warehouse_name = warehouse_name;
    }


}

