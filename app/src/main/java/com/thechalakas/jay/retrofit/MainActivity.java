package com.thechalakas.jay.retrofit;

import android.database.Observable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class MainActivity extends AppCompatActivity
{

    //Creating the Retrofit instance
    // Trailing slash is needed
    public static final String BASE_URL = "http://simplewebapi1webapp1.azurewebsites.net/";
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //use the interface and create an endpoint
        MyApiEndpointInterface myApiEndpointInterface = retrofit.create(MyApiEndpointInterface.class);
        //list of warehouses

        Call<List<Warehouse>> call = myApiEndpointInterface.warehouses();

        call.enqueue(new Callback<List<Warehouse>>()
        {
            @Override
            public void onResponse(Call<List<Warehouse>> call, Response<List<Warehouse>> response)
            {
                Log.i("MainActivity","call.enqueue - onResponse");
                Log.i("MainActivity","Response details - " + response.toString());
                List<Warehouse> list = response.body();
                Log.i("MainActivity","data details - " + response.body().toString());
            }

            @Override
            public void onFailure(Call<List<Warehouse>> call, Throwable t)
            {
                Log.i("MainActivity","call.enqueue - onFailure");
            }
        });

        //add a warehouse
        WarehouseSend warehouseSend = new WarehouseSend();

        warehouseSend.setId(101);
        warehouseSend.setWarehouse_Name("android warehouse");

        //Observable<WarehouseSend> observable = myApiEndpointInterface.addwarehouse(warehouseSend);
        Call<WarehouseSend> call1 = myApiEndpointInterface.addwarehouse(warehouseSend);

        call1.enqueue(new Callback<WarehouseSend>()
        {
            @Override
            public void onResponse(Call<WarehouseSend> call, Response<WarehouseSend> response)
            {
                Log.i("MainActivity","call1.enqueue - Success");

            }

            @Override
            public void onFailure(Call<WarehouseSend> call, Throwable t)
            {
                Log.i("MainActivity","call1.enqueue - onFailure");
            }
        });

    }

    public interface MyApiEndpointInterface
    {

        @GET("api/Warehouses")
        Call<List<Warehouse>> warehouses();

        //@POST("api/Warehouses")
        //Observable<WarehouseSend> addwarehouse(@Body WarehouseSend warehouseSend);

        @POST("api/Warehouses")
        Call<WarehouseSend> addwarehouse(@Body WarehouseSend warehouseSend);
    }
}